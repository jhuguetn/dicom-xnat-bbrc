/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.ImageScanAttributes
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;


import org.dcm4che2.data.Tag;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

import static org.nrg.dcm.Attributes.*;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
final class ImageScanAttributes {
	private ImageScanAttributes() {} // no instantiation

	static public AttrDefs get() { return s; }

	static final private MutableAttrDefs s = new MutableAttrDefs();

	static {
		s.add("ID");    // handled by session builder
		s.add("UID", Tag.SeriesInstanceUID);

		s.add("series_description", Tag.SeriesDescription);
		s.add("scanner", Tag.StationName);
		s.add("scanner/manufacturer", Tag.Manufacturer);
		s.add("scanner/model", Tag.ManufacturerModelName);
		s.add(new XnatAttrDef.Time("startTime", SeriesTime));
	}
}

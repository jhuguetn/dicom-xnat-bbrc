/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.DICOMScanBuilderTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dcm.xnat;

import com.google.common.collect.ImmutableSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.xdat.bean.XnatImagescandataBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.nrg.dcm.Attributes.*;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public class DICOMScanBuilderTest extends Scan4TestCase {
    private File scan4Dir = null;

    @Before
    public void setUp() throws IOException {
        scan4Dir = initializeScan4WorkingCopy();
    }

    @After
    public void tearDown() {
        tearDownWorkingCopy(scan4Dir);
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.DICOMScanBuilder#getNativeTypeAttrs()}.
     */
    @Test
    public void testGetNativeTypeAttrs() {
        final ImmutableSet<DicomAttributeIndex> attrs = DICOMScanBuilder.getNativeTypeAttrs();
        assertTrue(attrs.contains(SeriesInstanceUID));
        assertTrue(attrs.contains(SeriesTime));
        assertFalse(attrs.contains(StudyInstanceUID));
    }

    @Test
    public void testFromScanDirectoryWithRelativePaths() throws IOException,SQLException,URISyntaxException {
        final XnatImagescandataBean scan = DICOMScanBuilder.buildScanFromDirectory(scan4Dir, scan4Dir, "4", true);
        final XnatResourcecatalogBean catResource = (XnatResourcecatalogBean)scan.getFile().get(0);
        assertEquals("DICOM", catResource.getFormat());
        assertEquals("DICOM", catResource.getLabel());
        assertEquals("RAW", catResource.getContent());
        final String catalogPath = catResource.getUri();
        assertFalse(catalogPath.startsWith("/"));
        final File catalogFile = new File(scan4Dir, catalogPath);
        assertTrue(catalogFile.exists());
    }

    @Test
    public void testFromScanDirectoryWithAbsolutePaths() throws IOException,SQLException,URISyntaxException {
        final XnatImagescandataBean scan = DICOMScanBuilder.buildScanFromDirectory(scan4Dir, scan4Dir, "4", false);
        final XnatResourcecatalogBean catResource = (XnatResourcecatalogBean)scan.getFile().get(0);
        assertEquals("DICOM", catResource.getFormat());
        assertEquals("DICOM", catResource.getLabel());
        assertEquals("RAW", catResource.getContent());
        final String catalogPath = catResource.getUri();
        assertTrue(catalogPath.matches("^([A-z]:[/\\\\\\\\].*)|(/.*)$"));
        final File catalogFile = new File(catalogPath);
        assertTrue(catalogFile.exists());
    }
}
